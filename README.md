# valuation-api

This is a simple API that can be used to get price estimates
for properties based on the historical sales figures from Booli.

## Requirements

- Python 3.8+
- A Booli caller id and key

## Install

Start by setting up a virtual environment:

```sh
$ python3 -m venv env/
$ . env/bin/activate
```

Then, install the required Python libraries:

```sh
$ pip install -r requirements.txt
```

## Test

Run the application tests using pytest:

```sh
$ make test
```

## Run

To run the application, first create an `.env` file containing the Booli API credentials

`.env`:
```
BOOLI_CALLER_ID=<MY_ID>
BOOLI_KEY=<MY_KEY>
```

Then run the application:

```sh
$ make run
```

## Usage

The application provides an HTTP API with the following endpoints:

- `GET /priceEstimate?latitude=<LAT>&longitude=<LGN>&livingArea=<AREA>[&rent=<RENT>]` 

For information on the API, please start the application and have a
look at the API documentation available at:

- http://localhost:8000/redoc

For interactive documentation (Swagger UI), visit:

- http://localhost:8000/doc

## Examples

Fetch a price estimate for a 83 square meeter apartment at Harry Martinssons gata 13:

```
GET /priceEstimate?latitude=59.34018457480836&longitude=18.007663605506472&livingArea=83
```

```
{
  "query": {
    "latitude": 59.34018457480836,
    "longitude": 18.007663605506472,
    "livingArea": 83,
    "rent": null
  },
  "estimate": 6762459,
  "error": null
}
```

Fetch a price estimate for a house in the middle of the ocean:

```
GET /priceEstimate?latitude=58.493204863998976&longitude=19.833505929350864&livingArea=83
```

```
{
  "query": {
    "latitude": 58.493204863998976,
    "longitude": 19.833505929350864,
    "livingArea": 83,
    "rent": null
  },
  "estimate": null,
  "error": "No historic prices available, cannot compute estimate"
}
```

## Built using

![image](https://bcdn.se/images/resources/booli_logo.png)