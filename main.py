from typing import Optional
from fastapi import FastAPI, Query
from pydantic import BaseModel, BaseSettings
from prices.engine import Point, HouseSpec, ValuationEngine, NoHistoricPrices
from prices.loader import BooliLoader, HistoricPriceCache
from booli import Booli


class Settings(BaseSettings):
    booli_caller_id: str
    booli_key: str

    class Config:
        env_file = ".env"


settings = Settings()
app = FastAPI(
    title="Valuation API",
    description="Provides price estimates for properties based on historical sales figures from Booli.",
    version="0.1",
)

booli = Booli(settings.booli_caller_id, settings.booli_key)
loader = HistoricPriceCache(BooliLoader(booli).get_sold, path="app.bin")
engine = ValuationEngine(loader.get_sold)


class EstimateInput(BaseModel):
    latitude: float
    longitude: float
    livingArea: int
    rent: Optional[int]


class EstimationResult(BaseModel):
    query: EstimateInput
    estimate: Optional[int]
    error: Optional[str]


@app.on_event("startup")
def startup_event():
    loader.load()


@app.on_event("shutdown")
def shutdown_event():
    loader.save()


@app.get("/priceEstimate", response_model=EstimationResult)
def price_estimate(
    latitude: float,
    longitude: float,
    livingArea: int = Query(..., description="The living area in meters"),
    rent: Optional[int] = Query(None, description="The rent in SEK"),
) -> EstimationResult:
    """
    Calculates a price estimate for a property at a specific location and size.
    The location is specified using a WGS84-coordinate (latitude and longitude).
    Other optional parameters can also be specified in order to improve the
    estimate.
    """
    spec = HouseSpec(position=Point(latitude, longitude), livingArea=livingArea, rent=rent)
    estimate = None
    error = None
    try:
        estimate = engine.get_estimate(spec)
    except NoHistoricPrices as e:
        error = "No historic prices available, cannot compute estimate"

    return EstimationResult(
        estimate=estimate,
        error=error,
        query=EstimateInput(latitude=latitude, longitude=longitude, livingArea=livingArea, rent=rent),
    )
