import time
import requests
import random
import string
from hashlib import sha1
from types import SimpleNamespace
from typing import Tuple


class Booli:
    """
    Simple client for the Booli API.
    """

    def __init__(self, callerId: str, key: str):
        self._callerId = callerId
        self._key = key
        self.baseUrl = "https://api.booli.se"

    def get_sold(self, query: str = None, center: Tuple[float, float] = None, dim: Tuple[int, int] = None):
        """
        Returns the solr properties matching the given query.
        """
        if not query and not center:
            raise Exception("Please provide either a query and/or a center coordinate")

        timestamp = str(int(time.time()))
        unique = "".join(random.choice(string.ascii_uppercase + string.digits) for x in range(16))
        hashstr = sha1(str(self._callerId + timestamp + self._key + unique).encode("utf-8")).hexdigest()

        params = {}
        if query:
            params["q"] = query
        if center:
            params["center"] = ",".join(str(p) for p in center)
        if dim:
            params["dim"] = ",".join(str(p) for p in dim)
        params.update(
            {
                "callerId": self._callerId,
                "time": timestamp,
                "unique": unique,
                "hash": hashstr,
            }
        )
        headers = {"Accept": "application/vnd.booli-v2+json"}

        print(f"Requesting prices from Booli using {params}")

        r = requests.get(self.baseUrl + "/sold", params=params, headers=headers)
        if r.status_code != 200:
            raise Exception(f"Failed to retrieve listings from booli.se: {r.status_code}: {r.text}")

        return r.json(object_hook=lambda d: SimpleNamespace(**d)).sold
