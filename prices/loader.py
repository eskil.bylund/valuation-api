import pickle
from pathlib import Path
from dataclasses import dataclass
from typing import List, Tuple
from .engine import Point, HistoricPrice


class Loader:
    """
    Provides historic prices given a center coordinate and rect dimensions in meter.
    """

    def get_sold(self, center: Point, dim: Tuple[int, int] = None) -> List[HistoricPrice]:
        pass


class BooliLoader(Loader):
    """
    Provides historic prices from Booli.
    """

    def __init__(self, booli):
        self.booli = booli

    def get_sold(self, center: Point, dim: Tuple[int, int] = None) -> List[HistoricPrice]:
        sold = self.booli.get_sold(center=center.coords(), dim=dim)
        prices = [
            HistoricPrice(address=e.location.address.streetAddress, price=e.soldPrice, livingArea=e.livingArea)
            for e in sold
        ]
        return prices


class HistoricPriceCache(Loader):
    """
    Caches historic prices in a local SQLite database.
    On a cache miss, prices will be loaded via the given
    loader function.
    """

    def __init__(self, loader, path=None):
        self.loader = loader
        self.cache = {}
        self.store = Path(path) if path else None

    def save(self):
        """Saves any cached prices into a file on disk."""
        if not self.store:
            return
        with open(self.store, mode="wb") as f:
            pickle.dump(self.cache, f)

    def load(self):
        """Loads prices from the cache on disk."""
        if not self.store:
            return
        try:
            with open(self.store, mode="rb") as f:
                self.cache = pickle.load(f)
        except FileNotFoundError:
            pass

    def get_sold(self, center: Point, dim: Tuple[int, int] = None) -> List[HistoricPrice]:
        result = self.cache.get(center, None)

        if not result:
            for key, value in self.cache.items():
                if abs(key.long - center.long) <= 0.015 and abs(key.lat - center.lat) <= 0.01:
                    result = value
                    break

        if not result:
            result = self.loader(center)
            self.cache[center] = result

        return result
