"""
The house vaulation engine.

>>> spec = HouseSpec(position=Point(59.34674,18.0603), size=120, fee=5000)
>>> engine = ValuationEngine()
>>> estimate = engine.get_estimate(house)
"""
import statistics
from dataclasses import dataclass
from typing import Callable, List, Optional


@dataclass(eq=True, frozen=True)
class Point:
    lat: float
    long: float

    def coords(self):
        return (self.lat, self.long)


@dataclass
class HouseSpec:
    position: Point
    livingArea: int
    rent: Optional[int] = None


@dataclass
class HistoricPrice:
    address: str
    price: int
    livingArea: int


class NoHistoricPrices(Exception):
    """Raised if the engine is lacking data to compute an estimate."""

    pass


class ValuationEngine:
    """
    Compuates a valuation for a house given its position,
    size, and other properties.

    The vaulation is performed by loading historic house prices
    using the given loader function.
    """

    def __init__(self, loader: Callable[[Point], List[HistoricPrice]]):
        self.loader = loader

    def get_estimate(self, spec: HouseSpec) -> int:
        """
        Returns the estimated sales prices for a house matching the
        given specification. Returns the estimated price.
        """
        sold = self.loader(spec.position)
        if not sold:
            raise NoHistoricPrices()

        # Lets just use the average per square meter price
        prices = [e.price / e.livingArea for e in sold]
        estimate = statistics.mean(prices) * spec.livingArea
        return int(estimate)
