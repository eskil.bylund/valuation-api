.PHONY: run test install

run:
	uvicorn main:app --reload

test:
	python -m pytest

install:
	pip install -r requirements.txt

lint:
	black -l 120 *.py */*.py
	mypy *.py */*.py
