import time
import random
import requests
import pytest
from pathlib import Path
from booli import Booli

sample_sold = Path(__file__).resolve().parent.joinpath("sample_sold.json").read_text()


@pytest.fixture
def booli(monkeypatch):
    monkeypatch.setattr(time, "time", lambda: 123)
    monkeypatch.setattr(random, "choice", lambda x: "a")

    return Booli("Test", "abc123")


def test_sold_by_query(booli, requests_mock):
    requests_mock.get(
        "https://api.booli.se/sold?q=nacka&callerId=Test&time=123&unique=aaaaaaaaaaaaaaaa&hash=ee5f6aa244c02fb82fa79fbf66cbe35599318760",
        text=sample_sold,
    )

    sold = booli.get_sold(query="nacka")
    assert len(sold) == 1

    unit = sold[0]
    assert unit.objectType == "Lägenhet"
    assert unit.soldPrice == 1415000
    assert unit.location.address.streetAddress == "Skarpövägen 17"


def test_sold_by_center(booli, requests_mock):
    requests_mock.get(
        "https://api.booli.se/sold?center=59.34674,18.0603&callerId=Test&time=123&unique=aaaaaaaaaaaaaaaa&hash=ee5f6aa244c02fb82fa79fbf66cbe35599318760",
        text=sample_sold,
    )

    sold = booli.get_sold(center=(59.34674, 18.0603))
    assert len(sold) == 1

    unit = sold[0]
    assert unit.objectType == "Lägenhet"
    assert unit.soldPrice == 1415000
    assert unit.location.address.streetAddress == "Skarpövägen 17"


def test_sold_no_params(booli):
    with pytest.raises(Exception):
        booli.get_sold()


def test_sold_invalid_key(booli, requests_mock):
    requests_mock.get(
        "https://api.booli.se/sold?q=nacka&callerId=Test&time=123&unique=aaaaaaaaaaaaaaaa&hash=ee5f6aa244c02fb82fa79fbf66cbe35599318760",
        status_code=400,
        text="Invalid API key",
    )

    with pytest.raises(Exception):
        booli.get_sold("nacka")
