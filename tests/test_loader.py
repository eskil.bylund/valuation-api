import pytest
from prices.loader import BooliLoader, HistoricPriceCache
from prices.engine import Point, HistoricPrice
from test_booli import booli, sample_sold


def test_booli_loader(booli, requests_mock):
    requests_mock.get(
        "https://api.booli.se/sold?center=59.34674,18.0603&callerId=Test&time=123&unique=aaaaaaaaaaaaaaaa&hash=ee5f6aa244c02fb82fa79fbf66cbe35599318760",
        text=sample_sold,
    )

    loader = BooliLoader(booli)
    result = loader.get_sold(Point(59.34674, 18.0603))

    assert len(result) == 1
    assert result[0] == HistoricPrice(address="Skarpövägen 17", price=1415000, livingArea=61)


def test_cache_point():
    global requests
    requests = 0

    def loader(point):
        global requests
        requests += 1
        return [HistoricPrice(address="Skarpövägen 17", price=1415000, livingArea=61)]

    cache = HistoricPriceCache(loader)

    result = cache.get_sold(Point(59.34674, 18.0603))
    assert result[0] == HistoricPrice(address="Skarpövägen 17", price=1415000, livingArea=61)
    assert requests == 1

    cache.get_sold(Point(59.34674, 18.0603))
    cache.get_sold(Point(59.34674, 18.0603))

    result = cache.get_sold(Point(59.34674, 18.0603))
    assert result[0] == HistoricPrice(address="Skarpövägen 17", price=1415000, livingArea=61)
    assert requests == 1


def test_cache_area():
    global requests
    requests = 0

    def loader(point):
        global requests
        requests += 1
        return [HistoricPrice(address="Skarpövägen 17", price=1415000, livingArea=61)]

    cache = HistoricPriceCache(loader)

    result = cache.get_sold(Point(59.34674, 18.0603))
    assert result[0] == HistoricPrice(address="Skarpövägen 17", price=1415000, livingArea=61)
    assert requests == 1

    result = cache.get_sold(Point(59.34574, 18.0503))
    assert result[0] == HistoricPrice(address="Skarpövägen 17", price=1415000, livingArea=61)
    assert requests == 1


def test_persistence():
    global requests
    requests = 0

    def loader(point):
        global requests
        requests += 1
        return [HistoricPrice(address="Skarpövägen 17", price=1415000, livingArea=61)]

    # Populate
    cache = HistoricPriceCache(loader, path="prices.bin")
    result = cache.get_sold(Point(59.34674, 18.0603))
    cache.save()

    # Load from disk
    cache = HistoricPriceCache(loader, path="prices.bin")
    cache.load()

    # No additional requests should have been made
    result = cache.get_sold(Point(59.34674, 18.0603))
    assert result[0] == HistoricPrice(address="Skarpövägen 17", price=1415000, livingArea=61)
    assert requests == 1

    result = cache.get_sold(Point(59.34574, 18.0503))
    assert result[0] == HistoricPrice(address="Skarpövägen 17", price=1415000, livingArea=61)
    assert requests == 1
