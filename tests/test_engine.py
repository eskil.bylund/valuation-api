import pytest
from prices.engine import Point, HouseSpec, HistoricPrice, ValuationEngine, NoHistoricPrices


def test_valuation_no_data():
    def loader(point):
        assert point == Point(59.34674, 18.0603)
        return []

    engine = ValuationEngine(loader)
    spec = HouseSpec(position=Point(59.34674, 18.0603), livingArea=120, rent=5000)

    with pytest.raises(NoHistoricPrices):
        engine.get_estimate(spec)


def test_valuation_singe_data_point():
    def loader(point):
        assert point == Point(59.34674, 18.0603)
        return [HistoricPrice(address="Aprikosgatan 29", price=1_680_000, livingArea=73)]

    engine = ValuationEngine(loader)
    spec = HouseSpec(position=Point(59.34674, 18.0603), livingArea=120, rent=5000)

    estimate = engine.get_estimate(spec)
    assert estimate == 2_761_643


def test_valuation_multiple_data_point():
    def loader(point):
        assert point == Point(59.34674, 18.0603)
        return [
            HistoricPrice(address="Aprikosgatan 29", price=1_680_000, livingArea=73),
            HistoricPrice(address="Piongränd 2", price=1_280_000, livingArea=54),
            HistoricPrice(address="Vindruvsbacken 14", price=1_700_000, livingArea=54),
        ]

    engine = ValuationEngine(loader)
    spec = HouseSpec(position=Point(59.34674, 18.0603), livingArea=120, rent=5000)

    estimate = engine.get_estimate(spec)
    assert estimate == 3_127_955
